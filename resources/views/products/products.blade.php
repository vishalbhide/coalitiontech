@extends('app')
@section('content')
    <script>
        jQuery(document).ready(function() {
            Product.init();
        });
    </script>
    <br><br>
    <form id="products-form" role="form" method="post" accept-char="UTF-8" autocomplete="off" data-parsley-validate>
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="container">
        <!-- Tab panes -->
        <div class="row">
            <div class="col-xs-12 col-md-12 col-lg-12">
                <div id="product_success">
                </div>
                <div id="product_error">
                </div>
                <div class="form-group">
                    <label class="control-label" for="name">Product Name *</label>

                    <input class="form-control" type="text" name="name" id="name" value="" placeholder="Product Name"
                           required
                           autofocus
                           data-parsley-trigger="change"
                           data-parsley-error-message="Product Name is required">
                </div>

                <div class="form-group">
                    <label class="control-label" for="quantity">Quantity in Stock *</label>

                    <input class="form-control" type="text" name="quantity" id="quantity" value="" placeholder="Quantity in Stock"
                           required
                           autofocus
                           data-parsley-trigger="change"
                           data-parsley-error-message="Quantity is required">
                </div>

                <div class="form-group">
                    <label class="control-label" for="price">Price per item *</label>

                    <input class="form-control" type="text" name="price" id="price" value="" placeholder="Price per item"
                       required
                       autofocus
                       data-parsley-trigger="change"
                       data-parsley-error-message="Price is required">
                </div>

                    <div class="clearfix"></div>
                    <div class="center-align" ></div>

                    <div class="row">
                        <button class="btn btn-primary" type="submit">Save</button>
                    </div>
            </div>
            </div><!-- end tab-content -->
        </div>


    </form>

    <br><br>
        <div class="row">
            <div id="productsgrid" align="center"></div>
        </div>
    </div><!-- end container -->

    <br><br>
    <script>
        $(".products-form").validate();
    </script>
    <script>

        loadData();
    </script>


@endsection