<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en-US" style="overflow-x:hidden;">
<!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta name='viewport' content='width=device-width, initial-scale=1, maximum-scale=1' />
    <title>Products Catalog Management</title>
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!--CSS-->
    <link rel="stylesheet" href={{{URL::asset("/css/bootstrap.css")}}} type="text/css" media="all" />
    <link rel="stylesheet" href={{{URL::asset("/css/bootstrap-theme.css")}}} type="text/css" media="all" />
    <link rel="stylesheet" href={{{URL::asset("/css/font-awesome.css")}}} type="text/css" media="all" />

    <link rel="stylesheet" href='{{{URL::asset("/css/dataTables.tableTools.css")}}}' type="text/css" media="all" />
    <link rel="stylesheet" href={{{URL::asset("/css/dataTables.bootstrap.css")}}} type="text/css" media="all" />


    <!--JS-->
    <script type='text/javascript' src={{{URL::asset('/js/jquery-1.11.2.min.js')}}}></script>
    <script type='text/javascript' src={{{URL::asset("/js/bootstrap.js")}}}></script>

    <script src={{{URL::asset("/js/jquery.dataTables.min.js")}}}></script>
    <script src={{{URL::asset("/js/jquery.validate.js")}}}></script>
    <script src={{{URL::asset("/js/dataTables.bootstrap.js")}}}></script>
    <script src={{{URL::asset("/js/dataTables.tableTools.js")}}}></script>
    <script src={{{URL::asset("/js/products.js")}}}></script>
    <script src={{{URL::asset("/js/jquery.columns-1.0.min.js")}}}></script>

    <link id="style" rel="stylesheet" href="{{{URL::asset("/css/classic.css")}}}>
    @yield('head-content')
</head>

<body>
<div id="transition">

    <!--header:start-->
    <header>

        <nav class="navbar navbar-default">
            {{--<div class="container">--}}
            {{--<div class="row">--}}
            <div class="spacer10"></div>
            <div class="col-lg-12">
                <div class="col-xs-2 col-sm-2 col-md-2">

                </div>
                <div class="col-xs-10 col-sm-10 col-md-10">
                    <div class="spacer10"></div>
                    <div class="row">
                        <div class="col-sm-10 col-md-10" align = "center">
                            <h1 style="color: #234F94"><B>Products Catalog Management</B></h1>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    </header>
    <!--header:end-->
    @yield('content')


    <footer>
        <div class="container" >
            &nbsp;
        </div>
    </footer>
    <!--footer:end-->

    <script>
        $('div.alert.alert-success').not('.alert-important').delay(10000).slideUp(300);
    </script>

    @yield('footer')
</div>
</body>
</html>