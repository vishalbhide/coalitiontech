<?php namespace App\Models;

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model {

    protected $fillable = [
        'name',
        'quantity',
        'price'
    ];

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
}