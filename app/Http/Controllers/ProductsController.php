<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Log;
use Validator;
use Response;

class ProductsController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax())
        {
            return Product::all();
        }else{
            return view('products.products');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Log::info("Received product", [$request->inputs]);

        $validator = $this->getValidator($request);
        if($validator->fails()){
            $errors = json_decode($validator->errors());

            return response()->json(
                [
                    'success' => false,
                    'message' => $errors
                ],
                422);
        }

        $product = new Product();
        $product->name = $request->input('name');
        $product->quantity = $request->input('quantity');
        $product->price = $request->input('price');
        $id = $product->save();
        Log::info("Saved product", [$product->id]);
        return $product->id;
    }

    protected function getValidator($request){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'quantity' => 'required|numeric',
            'price' => 'required|numeric'
        ]);
        return $validator;
    }
}