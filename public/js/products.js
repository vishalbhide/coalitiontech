var Product = function() {

    var handleSave = function() {

        $('#products-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                name: {
                    required: true
                },
                quantity: {
                    required: true,
                    number: true
                },
                price: {
                    required: true,
                    number: true
                }
            },

            messages: {
                name: {
                    required: "Product Name is required."
                },
                quantity: {
                    required: "Quantity in Stock is required.",
                    number: "Quantity in Stock must be a valid number"
                },
                price: {
                    required: "Price per item is required.",
                    number: "Price per item must be a valid number"
                }
            },

            invalidHandler: function(event, validator) { //display error alert on form submit
                $('.alert-danger', $('.products-form')).show();
            },

            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function(error, element) {
                error.insertAfter(element.closest('.input-icon'));
            },

            submitHandler: function(form) {
                console.log("Submitting");
                jQuery.ajax( {
                    url: '/',
                    type: 'POST',
                    data: { name:document.getElementById('name').value,quantity: document.getElementById('quantity').value, price:document.getElementById('price').value},
                    success: function( response ) {
                        console.log(response);
                        $('#productsgrid').columns('destroy');
                        loadData();
                        $('#product_success').show().html('<div class="alert alert-success"> <button class="close" data-close="alert"></button> <span> Product Updated Successfully.</span> </div>').delay(3000).slideUp(300);

                    },
                    error: function( response ) {
                        var json = JSON.parse(response['responseText']);
                        var str = json.Error;
                        var res = '';
                        if(json.Error ) {
                            str.forEach(function(entry) {
                                if(entry != '') {
                                    res += entry + '<br>';
                                }
                                else {
                                    res += entry ;
                                }
                            });

                            $('#product_success').hide();
                            $('#product_error').show().html('<div class="alert alert-danger"> <button class="close" data-close="alert"></button> <span> '+ res +'</span> </div>').delay(3000).slideUp(300);
                        }
                        else
                        {
                            $('#product_success').hide();
                            $('#product_error').show().html('<div class="alert alert-danger"> <button class="close" data-close="alert"></button> <span> '+ json.errorMessage +'</span> </div>').delay(3000).slideUp(300);
                        }



                    }
                } );

               // form.submit(); // form validation success, call ajax form submit
            }
        });

        $('#products-form input').keypress(function(e) {
            if (e.which == 13) {
                if ($('.products-form').validate().form()) {
                 //   $('.products-form').submit(); //form validation success, call ajax form submit
                }
                return false;
            }
        });
    }

    return {
        //main function to initiate the module
        init: function() {
            handleSave();
        }

    };

}();

$('#products-form').submit(function(){
    e.preventDefault();
    })

function loadData() {

    $.ajax({
        url: '/',
        dataType: 'json',
        success: function (json) {
            productsgrid = $('#productsgrid').columns({
                data: json,
                schema: [
                    {"header": "ID&nbsp;", "key": "id"},
                    {"header": "Quantity&nbsp;", "key": "quantity"},
                    {"header": "Price&nbsp;", "key": "price"}
                ]

            });
        }
    });
}